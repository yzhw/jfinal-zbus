/**
 * Copyright (c) 2015, 玛雅牛［李飞］ (lifei@wellbole.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfinal.plugin.zbus;

import java.util.concurrent.ConcurrentHashMap;

import org.zstacks.zbus.client.Producer;

import com.jfinal.plugin.zbus.coder.Coder;

/**
 * @ClassName: Zbus
 * @Description: Zbus主要数据对象存储对象
 * @author 李飞 (lifei@wellbole.com)
 * @date 2015年7月29日 下午1:23:25
 * @since V1.0.0
 */
public class Zbus {

	/**
	 * 默认超时时间
	 */
	public static final int DEFAULT_TIMEOUT = 3000;

	/**
	 * 队列名对应到相应的消费者(MQ)
	 */
	private static final ConcurrentHashMap<String, Producer> producerMap = new ConcurrentHashMap<String, Producer>();
	
	
	/**
	 * 编码解码器
	 */
	private static Coder coder;

	/**
	 * 禁止初始化
	 * 
	 * @since V1.0.0
	 */
	private Zbus() {

	}

	/**
	 * @Title: init
	 * @Description: 初始化（包内访问）
	 * @param producerMap
	 * @param coder
	 * @since V1.0.0
	 */
	static final void init(ConcurrentHashMap<String, Producer> producerMap, Coder coder) {
		Zbus.producerMap.putAll(producerMap);
		Zbus.coder = coder;
	}

	/**
	 * @Title: findProducerBy
	 * @Description: 依据mq找到对应的 Producer（包内访问）
	 * @param mq
	 * @return
	 * @since V1.0.0
	 */
	public static final Producer findProducerBy(String mq) {
		return Zbus.producerMap.get(mq);
	}

	/**
	 * @Title: findProducerBy
	 * @Description: 依据mq和topic找到对应的 Producer（包内访问）
	 * @param mq
	 * @param topic
	 * @return
	 * @since V1.0.0
	 */
	public static final Producer findProducerBy(String mq, String topic) {
		String key = "_mq_" + mq + "_topic_" + topic;
		return Zbus.producerMap.get(key);
	}

	/**  
	 * 获得编码解码器
	 * @return coder 
	 * @since V1.0.0 
	 */
	public static final Coder getCoder() {
		return coder;
	}
	
	
}
